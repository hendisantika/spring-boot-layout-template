package com.hendisantika.layouttemplate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLayoutTemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLayoutTemplateApplication.class, args);
    }

}

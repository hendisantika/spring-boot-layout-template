package com.hendisantika.layouttemplate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-layout-template
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 16/06/20
 * Time: 05.45
 */
@Controller
public class IndexController {

    @GetMapping("/")
    public String showIndex() {
        return "index";
    }
}

# Spring Boot Layout Template
### Thingss todo list:
1. Clone this repository: `https://gitlab.com/hendisantika/spring-boot-layout-template.git`
2. Go inside the folder: `cd spring-boot-layout-template`
3. Run the application: `mvn clean spring-boot:run`
4. Open your favorite browser: http://localhost:8080

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")